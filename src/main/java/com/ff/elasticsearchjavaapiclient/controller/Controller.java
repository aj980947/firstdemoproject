package com.ff.elasticsearchjavaapiclient.controller;

import com.ff.elasticsearchjavaapiclient.model.Product;
import com.ff.elasticsearchjavaapiclient.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.fluentd.logger.FluentLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Slf4j
public class Controller {
    @Autowired
    private ClientService service;
   private final FluentLogger fluentLogger=FluentLogger.getLogger("sky");
   @GetMapping("/hello")
   public String thisIsJustNothing(){
       log.info("Im trying to change");
       return "really nothing";

   }
   @GetMapping("/true")
   public void method(){
       log.info("current time "+LocalDateTime.now());
   }

   @GetMapping("/dummy")
   public void dumy(){
       log.info("hello", LocalDateTime.now());
   }
    @PostMapping("/")
    public String createIndex(@RequestParam String index) throws IOException {
        log.info("controller layer method called");
        String index1=service.createIndex(index);
        log.info("index is created with name "+index);
        return index1;
    }
    @PostMapping("/createDocument")
    public String createDocument(@RequestParam String index, @RequestBody Product product) throws IOException {
        return service.createDocument(index,product);
    }
    @GetMapping("/")
    public Product getProduct(@RequestParam String index,@RequestParam String id) throws IOException {
        return service.getProduct(index,id);
    }
    @PostMapping("/update/{id}")
    public String updateProduct(@RequestParam String index,@PathVariable String id, @RequestBody Product product) throws IOException {
        return service.updateProduct(index,id,product);
    }
    @DeleteMapping("/")
    public String deleteProduct(@RequestParam String index,@RequestParam String id) throws IOException {
        return service.deleteProduct(index,id);
    }
    @GetMapping("/getProductByName")
    public List<Product> getAllProduct(@RequestParam String name) throws IOException {
        return service.getProductsByName(name);
    }
    @GetMapping("/getProductByPriceRange")
    public List<Product> getProductByPriceRange(@RequestParam double from , @RequestParam double to) throws IOException {
        return service.getProductsByPriceRange(from,to);

    }
    @GetMapping("/getProductByMatchPhrase")
    public List<Product> getProductByMatchPhrase(String desc) throws IOException {
        return service.getProductByPraseMatch(desc);
    }
    @GetMapping("/getProductByMatch")
    public List<Product> getProductByMatch(String desc) throws IOException {
        return service.getProductByPhrase(desc);
    }
    @GetMapping("/logs")
    public String logs(){
        Map map=new HashMap<>();
        map.put("id","");
        fluentLogger.log("first",map);
        log.info("this is first log from controller");
        return "ok";
    }
}
