package com.ff.elasticsearchjavaapiclient.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Product {
    private String id;
    private String name;
    private double price;
    private String desc;
}
