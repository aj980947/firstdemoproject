package com.ff.elasticsearchjavaapiclient.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LowLevelRestClient {
    String serverUrl = "http://localhost:9200";
    // Create the low-level RestClient
    @Bean
    public RestClient restClient() {
        return RestClient.builder(HttpHost.create(serverUrl)).build();
    }
    // Create the transport with a Jackson mapper
    @Bean
    public RestClientTransport transport(RestClient restClient) {
        return new RestClientTransport(restClient, new JacksonJsonpMapper());
    }
    // And create the API client
    @Bean
    public ElasticsearchClient elasticsearchClient(RestClientTransport transport) {
        return new ElasticsearchClient(transport);
    }
}
