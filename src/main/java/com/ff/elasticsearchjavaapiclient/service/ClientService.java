package com.ff.elasticsearchjavaapiclient.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import com.ff.elasticsearchjavaapiclient.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class ClientService {
    @Autowired
    private RestClient restClient;
    @Autowired
    private ElasticsearchClient elasticsearchClient;



    public String createIndex( String index) throws IOException {
        CreateIndexResponse response = elasticsearchClient.indices().create(c -> c.index(index));
        log.info("service layer method called");
        return ""+response.index();
    }

    public String createDocument(String index, Product product) throws IOException {

        IndexResponse response = elasticsearchClient.index(c -> c.index(index).document(product).id(product.getId()));
        return response.version()+ " created";
    }

    public Product getProduct(String index, String id) throws IOException {
        GetRequest request=new GetRequest.Builder().id(id).index(index).build();
        return elasticsearchClient.get(request, Product.class).source();
    }

    public String updateProduct(String index, String id, Product product) throws IOException {
        UpdateResponse response=elasticsearchClient.update(u-> u.id(id).doc(product).index(index), Product.class);
        return response.version()+" updated";
    }

    public String deleteProduct(String index, String id) throws IOException {
        DeleteResponse response= elasticsearchClient.delete(d-> d.id(id).index(index));
        return response.version()+" deleted";
    }

    public List<Product> getProductsByName(String name) throws IOException {
        Query query= MultiMatchQuery.of(
                m->m
                        .query(name)
                        .fields("name")
        )._toQuery();
       SearchResponse<Product> response=elasticsearchClient.search(
                s->s.query(
                        q->q.bool(
                                b->b
                                        .must(query)
                        )
                ), Product.class
        );
       List<Product> list=new ArrayList<>();
       for(Hit hit: response.hits().hits()){
           list.add ((Product) hit.source());
       }
       return list;
    }
    public List<Product> getProductsByPriceRange(double from , double to) throws IOException {
        SearchResponse<Product> response=elasticsearchClient.search(
                s->s.query(
                        q->q.range(
                                r->r
                                        .field("price")
                                        .from(String.valueOf(from))
                                        .to(String.valueOf(to))
                        )
                ), Product.class
        );
        List<Product> list=new ArrayList<>();
        for(Hit hit:response.hits().hits()){
            list.add((Product) hit.source());
        }
        return list;
    }

    public List<Product> getProductByPraseMatch(String desc) throws IOException {
        Query query= MatchQuery.of(
               m->m.
                       query(desc)
                       .field("desc")
        )._toQuery();
        SearchResponse<Product> response = elasticsearchClient.search(
                s->s.query(query), Product.class
        );
        List<Product> list=new ArrayList<>();
        for(Hit hit:response.hits().hits()){
            list.add((Product) hit.source());
        }
        return list;
    }
    public List<Product> getProductByPhrase(String desc) throws IOException {
        Query query= MultiMatchQuery.of(
                m->m.
                        query(desc)
                        .fields("name","desc","price")
                        .type(TextQueryType.MostFields)
        )._toQuery();
        Query query1 = MatchPhraseQuery.of(
                m -> m
                        .query(desc)
                        .field(desc)
        )._toQuery();
        SearchResponse<Product> response = elasticsearchClient.search(
                s->s.query(q -> q
                        .bool(b -> b
                                .must(query)
                                .should(query1))), Product.class
        );
        List<Product> list=new ArrayList<>();
        for(Hit hit:response.hits().hits()){
            list.add((Product) hit.source());
        }
        return list;
    }









}
